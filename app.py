from flask import Flask, jsonify, request
from sklearn.externals import joblib
import pandas as pd
import numpy as np
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route('/predict', methods=['POST'])
def predict():
     clf = joblib.load('model.pkl')
     json_ = request.json
     query_df = pd.DataFrame(json_, index=[0])
     prediction = clf.predict(np.array(query_df))
     return pd.Series(prediction).to_json(orient='values')


app.run()
